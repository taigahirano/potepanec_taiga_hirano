require 'rails_helper'

RSpec.describe Potepan::HomeController, type: :controller do
  describe "index" do
    let(:product_yesterday)  { create(:product, available_on: Time.zone.now.yesterday) }
    let(:product_2_days_ago) { create(:product, available_on: 2.days.ago) }
    let(:product_3_days_ago) { create(:product, available_on: 3.days.ago) }

    it 'assigns @new_products correctly' do
      get :index
      expect(assigns(:new_products)).to match [
        product_yesterday,
        product_2_days_ago,
        product_3_days_ago,
      ]
    end
  end
end
