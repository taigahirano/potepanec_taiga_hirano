require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "#show" do
    let(:taxon) { create(:taxon) }
    let(:products) do
      create_list(:product, 3) do |product|
        product.taxons << taxon
      end
    end
    let!(:option_type_color) { create(:option_type, name: 'tshirt-color') }
    let!(:option_type_size) { create(:option_type, name: 'tshirt-size') }

    before do
      get :show, params: { id: taxon.id }
    end

    it "responds successfully" do
      expect(response).to be_success
    end

    it "returns a 200 response" do
      expect(response).to have_http_status "200"
    end

    it 'assigns @taxon' do
      expect(assigns(:taxon)).to eq taxon
    end

    it 'assigns @products' do
      expect(assigns(:products)).to match_array products
    end
  end
end
