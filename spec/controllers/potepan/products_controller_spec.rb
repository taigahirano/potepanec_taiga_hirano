require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:four_related_products) { create_list(:product, 4, taxons: [taxon]) }

    it "responds successfully" do
      get :show, params: { id: product.id }
      expect(response).to be_success
    end

    it "returns a 200 response" do
      get :show, params: { id: product.id }
      expect(response).to have_http_status "200"
    end

    it 'assigns @product' do
      get :show, params: { id: product.id }
      expect(assigns(:product)).to eq product
    end

    it 'assigns @related_products correctly when the number of product is 4' do
      get :show, params: { id: product.id }
      expect(assigns(:related_products)).to match_array four_related_products
    end

    it 'assigns four @related_products' do
      get :show, params: { id: product.id }
      expect(assigns(:related_products).count).to eq 4
    end
  end
end
