require 'rails_helper'

RSpec.feature "Potepan::Home", type: :feature do
  given!(:taxon_clothing) { create(:taxon, name: "Clothing") }
  given!(:taxon_bags)  { create(:taxon, name: "Bags") }
  given!(:taxon_mugs)  { create(:taxon, name: "Mugs") }
  given!(:new_product)  { create(:product, available_on: 2.days.ago) }
  given!(:image_path) { File.join(Rails.root, 'spec/fixtures/sample.jpg') }
  given!(:image) { Rack::Test::UploadedFile.new(image_path) }
  given!(:option_type_color) { create(:option_type, name: 'tshirt-color') }
  given!(:option_type_size) { create(:option_type, name: 'tshirt-size') }

  background do
    new_product.images.create(attachment: image)
  end

  scenario "user clicks popular categories links" do
    visit potepan_index_path
    click_on "Clothings"
    expect(page).to have_content "Clothing"

    visit potepan_index_path
    click_on "Bags"
    expect(page).to have_content "Bags"

    visit potepan_index_path
    click_on "Mugs"
    expect(page).to have_content "Mugs"
  end

  scenario "user clicks new products" do
    visit potepan_index_path
    expect(page).to have_content "新着商品"
    expect(page).to have_content new_product.name
    click_on new_product.name
    expect(page).to have_content new_product.name
    expect(page).to have_content "カートへ入れる"
  end
end
