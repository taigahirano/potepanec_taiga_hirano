require 'rails_helper'

RSpec.feature "Potepan::Categories", type: :feature do
  given!(:taxon_clothing) { create(:taxon, name: "Clothing") }
  given!(:taxon_bags) { create(:taxon, name: "Bags") }
  given!(:taxon_mugs) { create(:taxon, name: "Mugs") }
  given!(:shirt) do
    create(:product, name: "shirt",
                     taxons: [taxon_clothing],
                     option_types: [option_type_color],
                     available_on: Time.zone.now.yesterday,
                     price: 10)
  end
  given!(:jacket) do
    create(:product, name: "jacket",
                     taxons: [taxon_clothing],
                     available_on: 2.days.ago,
                     price: 20)
  end
  given!(:bag) { create(:product, name: "bag", taxons: [taxon_bags]) }
  given!(:image_path) { File.join(Rails.root, 'spec/fixtures/sample.jpg') }
  given!(:image) { Rack::Test::UploadedFile.new(image_path) }
  given!(:option_type_color) { create(:option_type, name: 'tshirt-color') }
  given!(:option_type_size) { create(:option_type, name: 'tshirt-size') }
  given!(:option_value_red) { create(:option_value, name: 'Red') }
  given!(:option_value_samll) { create(:option_value, name: 'Small') }

  background do
    shirt.images.create(attachment: image)
    jacket.images.create(attachment: image)
    bag.images.create(attachment: image)
  end

  scenario "user visits categories page and click sort links" do
    visit potepan_index_path

    click_link "Clothings"
    expect(page).to have_content shirt.name
    expect(page).to have_content jacket.name
    expect(page).not_to have_content bag.name
  end

  context "sorting" do
    scenario "user sorts products" do
      visit "/potepan/categories/#{taxon_clothing.id}"
      product_captions = page.all(".productCaption")
      expect(product_captions[0].find('h5').text).to  eq shirt.name
      expect(product_captions[1].find('h5').text).to  eq jacket.name

      visit "/potepan/categories/#{taxon_clothing.id}?order=oldest"
      expect(product_captions[1].find('h5').text).to  eq jacket.name
      expect(product_captions[0].find('h5').text).to  eq shirt.name

      visit "/potepan/categories/#{taxon_clothing.id}?order=price-asc"
      expect(product_captions[0].find('h5').text).to  eq shirt.name
      expect(product_captions[1].find('h5').text).to  eq jacket.name

      visit "/potepan/categories/#{taxon_clothing.id}?order=price-dsc"
      expect(product_captions[1].find('h5').text).to  eq jacket.name
      expect(product_captions[0].find('h5').text).to  eq shirt.name
    end
  end

  context "filterling" do
    given(:red_small_shirt) do
      create(:variant,
             option_values: [option_value_red, option_value_samll])
    end
    background do
      red_small_shirt.images.create(attachment: image)
    end
    scenario "user filters products" do
      visit('/products?color=Red')
      expect(page).to have_content red_small_shirt.name

      visit('/products?size=Small')
      expect(page).to have_content red_small_shirt.name
    end
  end
end
