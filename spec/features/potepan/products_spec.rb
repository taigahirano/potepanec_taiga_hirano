require 'rails_helper'

RSpec.feature "Potepan::Products", type: :feature do
  given!(:blue_shirt) { create(:product, name: "BLUE SHIRT") }
  given!(:red_shirt) { create(:product, name: "RED SHIRT") }
  given!(:taxon_shirt) { create(:taxon) }
  given!(:image_path) { File.join(Rails.root, 'spec/fixtures/sample.jpg') }
  given!(:image) { Rack::Test::UploadedFile.new(image_path) }

  background do
    blue_shirt.taxons << taxon_shirt
    red_shirt.taxons << taxon_shirt
    blue_shirt.images.create(attachment: image)
    red_shirt.images.create(attachment: image)
  end

  scenario "user visits product page" do
    visit potepan_product_path(blue_shirt.id)
    # 商品名が表示されているかどうかの確認
    expect(page).to have_content blue_shirt.name
    # 関連商品が表示されているかどうかの確認
    expect(page).to have_content red_shirt.name
  end
end
