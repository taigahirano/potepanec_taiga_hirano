Spree::Variant.class_eval do
  scope :includes_price, -> { includes(:product, :default_price) }
end
