Spree::Product.class_eval do
  scope :includes_price_and_images,           -> { includes(master: [:default_price, :images]) }
  scope :same_taxons_product,                 -> (product) {
                                                joins(:taxons).
                                                  where(spree_taxons: { id: product.taxons.ids }).
                                                  distinct
                                              }
  scope :exclude_product,                     -> (product) { where.not(id: product.id) }
  scope :filter_by_option,                    -> (option) {
    if option[:option_value].present?
      joins(variants: :option_values).
        where(spree_option_values: { name: option[:option_value] })
    end
  }
  scope :sort_descend_by_price,               -> { order("spree_prices.amount DESC") }
  scope :sort_ascend_by_price,                -> { order("spree_prices.amount ASC") }
  scope :sort_newest_by_product_available_on, -> { order("spree_products.available_on DESC") }
  scope :sort_oldest_by_product_available_on, -> { order("spree_products.available_on ASC") }
  scope :order_by,                            ->(order) {
                                                case order
                                                when 'price-dsc' then sort_descend_by_price
                                                when 'price-asc' then sort_ascend_by_price
                                                when 'oldest'
                                                   then sort_oldest_by_product_available_on
                                                else sort_newest_by_product_available_on
                                                end
                                              }
end
