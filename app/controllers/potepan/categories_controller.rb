class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @taxonomies = Spree::Taxonomy.all.includes(root: :children)
    @products = if params[:order]
                  Spree::Product.in_taxon(@taxon).
                    includes_price_and_images.
                    filter_by_option(option_value: params[:option_value]).
                    reorder(nil).
                    order_by(params[:order])
                else
                  Spree::Product.in_taxon(@taxon).
                    includes_price_and_images.
                    filter_by_option(option_value: params[:option_value])
                end
    @list_of_colors = Spree::OptionType.find_by(name: 'tshirt-color').option_values
    @list_of_sizes = Spree::OptionType.find_by(name: 'tshirt-size').option_values
  end
end
