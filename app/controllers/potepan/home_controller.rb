class Potepan::HomeController < ApplicationController
  THE_NUMBER_OF_NEW_PRODUCTS = 10

  def index
    @new_products = Spree::Product.
      includes_price_and_images.
      order(available_on: :desc).
      limit(THE_NUMBER_OF_NEW_PRODUCTS)

    @clothing = Spree::Taxon.where(name: "Clothing")
    @bags = Spree::Taxon.where(name: "Bags")
    @mugs = Spree::Taxon.where(name: "Mugs")
  end
end
