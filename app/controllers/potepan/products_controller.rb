class Potepan::ProductsController < ApplicationController
  THE_NUMBER_OF_RELATED_PRODUCTS = 4

  def show
    if Spree::Product.where(id: params[:id]).present?
      @product = Spree::Product.find(params[:id])
      @related_products = Spree::Product.includes_price_and_images.
        same_taxons_product(@product).
        exclude_product(@product).
        limit(THE_NUMBER_OF_RELATED_PRODUCTS)
    else
      @product = Spree::Variant.find(params[:id])
      @related_products = Spree::Product.includes_price_and_images.
        same_taxons_product(@product.product).
        exclude_product(@product).
        limit(THE_NUMBER_OF_RELATED_PRODUCTS)
    end
  end

  def index
    @option_value = Spree::OptionValue.find_by(name: params[:option])
    @products = @option_value.variants.includes_price
    @taxon = Spree::Taxon.first
    @taxonomies = Spree::Taxonomy.all.includes(root: :children)
    @list_of_colors = Spree::OptionType.find_by(name: 'tshirt-color').option_values
    @list_of_sizes = Spree::OptionType.find_by(name: 'tshirt-size').option_values
  end
end
